const User = require('../models/User');

const wsCheckUserToken = async (ws, req, next) => {
    const token = req.query.token;
    if (token) {
        try {
            const user = await User.findOne({token});
            if (user) req.user = user;
        } catch (err) {
            console.log(err);
        }
    }
    next();
}

module.exports = wsCheckUserToken;