const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const authUser = require('../middleware/authUser');
const User = require('../models/User');
const {nanoid} = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
    const {username, password, email} = req.body;
    const preUserData = {username, password, email};
    if (req.file) preUserData.image = '/uploads/' + req.file.filename;
    const user = new User(preUserData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);

    } catch (err) {
        if (err?.errors) return res.status(400).send(err.errors);
        res.status(500).send({error: 'Internal Server Error'});
    }
});

router.post('/sessions', authUser, async (req, res) => {
    try {
        req.user.generateToken();
        await req.user.save({validateBeforeSave: false});
        res.send(req.user);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Not really Success'};
    if (!token) return res.send({message: 'No token Success'});
    const user = await User.findOne({token});
    if (!user) return res.send(success);
    try {
        user.generateToken();
        await user.save({validateBeforeSave: false});
        return res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

module.exports = router;