const express = require('express');
const router = express.Router();
const {nanoid} = require("nanoid");
const wsCheckUserToken = require('../middleware/wsCheckUserToken');
const Message = require("../models/Message");
const User = require("../models/User");
const activeConnections = {anonymous: {}, users: {}, usersData: {}};

router.ws('/', wsCheckUserToken, async (ws, req) => {
    const sendOnline = () => {
        Object.keys(activeConnections.users).forEach(key => {
            Object.keys(activeConnections.users[key]).forEach(id => {
                const connection = activeConnections.users[key][id];
                connection.send(JSON.stringify({type: 'ONLINE_USERS', onlineUsers: getOnlineUsers()}));
            })
        })

        Object.keys(activeConnections.anonymous).forEach(key => {
            const connection = activeConnections.anonymous[key];
            connection.send(JSON.stringify({type: 'ONLINE_USERS', onlineUsers: getOnlineUsers()}));
        })
    }
    const sendMsgsToUsers = (data) => {
        Object.keys(activeConnections.users).forEach(key => {
            Object.keys(activeConnections.users[key]).forEach(id => {
                const connection = activeConnections.users[key][id];
                connection.send(JSON.stringify({type: 'MESSAGES', data}));
            })
        })

        Object.keys(activeConnections.anonymous).forEach(key => {
            const connection = activeConnections.anonymous[key];
            connection.send(JSON.stringify({type: 'MESSAGES', data}));
        })
    }
    const getOnlineUsers = () => {
        return {
            users: activeConnections.usersData,
            anonymous: {
                count: Object.keys(activeConnections.anonymous).length,
            },
        };
    }

    const getMessages = async () => {
        const promisesData = [];
        promisesData.push(Message
            .find({recipient: null})
            .populate('author', 'username image')
            .limit(20)
            .sort({datetime: -1}));
        if (req.user) {
            promisesData.push(Message
                .find({recipient: req.user._id})
                .populate('author', 'username image')
                .limit(20)
                .sort({datetime: -1}));
            promisesData.push(Message
                .find({author: req.user._id})
                .$where(function () {
                    return this.recipient;
                })
                .populate('author', 'username image')
                .limit(20)
                .sort({datetime: -1}));
        }
        const messagesData = await Promise.all(promisesData);


        let messages = [];
        messagesData.forEach(arr => {
            messages = messages.concat(arr);
        })
        return messages.sort((a, b) => {
            return (a.datetime < b.datetime) ? -1 : ((a.datetime > b.datetime) ? 1 : 0);
        });
    };

    const id = nanoid();

    if (req.user) {
        if (!activeConnections.users[req.user._id]) {
            activeConnections.users[req.user._id] = {};
        }
        const userConnections = activeConnections.users[req.user._id];
        if (Object.keys(userConnections).length === 0) {
            activeConnections.usersData[req.user._id] = {
                _id: req.user._id,
                username: req.user.username,
                image: req.user.image,
            };
        }
        userConnections[id] = ws;
    } else {
        activeConnections.anonymous[id] = ws;
    }

    sendOnline();

    ws.send(JSON.stringify({type: 'MESSAGES', data: await getMessages()}));

    ws.on('message', async msg => {
        const parsedMsg = JSON.parse(msg);
        switch (parsedMsg.type) {
            case "NEW_MESSAGE":
                try {
                    const recipient = await User.findById(parsedMsg.data?.recipient)
                    const newMsg = new Message({...parsedMsg.data, recipient});
                    await newMsg.save();
                    sendMsgsToUsers(await getMessages());
                } catch (err) {
                    console.log(err);
                }
                break;
        }
    })

    ws.on('close', (msg) => {
        if (req.user) {
            const userConnections = activeConnections.users[req.user._id];
            delete userConnections[id];
            if (Object.keys(userConnections).length === 0) {
                delete activeConnections.usersData[req.user._id];
                delete activeConnections.users[req.user._id];
            }
        } else {
            delete activeConnections.anonymous[id];
        }
        sendOnline();
    });
});


module.exports = router;