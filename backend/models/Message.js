const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const MessageSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'User ID is required'],
        ref: 'users',
    },
    recipient: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
    },
    message: {
        type: String,
        required: [true, 'Message required'],
    },
    room: {
        type: String,
        required: [true, 'RoomId required'],
        default: 'roomId',
    },
    datetime: {
        type: String,
        default: new Date().toISOString(),
    },
})

MessageSchema.plugin(idValidator);
const Message = mongoose.model('messages', MessageSchema);
module.exports = Message;