const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Message = require("./models/Message");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [user1, user2] = await User.create({
        username: 'tester',
        password: 'test',
        email: 'user@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'moder',
        password: '123',
        email: 'moder@good.com',
        image: '/fixtures/moder.jpg',
        role: 'moderator',
        token: nanoid(),
    })
    const [msg1, msg2] = await Message.create({
        author: user1,
        message: 'all1',
    }, {
        author: user2,
        message: 'all2',
    }, {
        author: user1,
        recipient: user2,
        message: 'user2',
    }, {
        author: user2,
        recipient: user1,
        message: 'user1',
    },)

    await mongoose.connection.close();
}
run().catch(console.error)
