import React, {useCallback, useEffect} from 'react';
import Box from "@mui/material/Box";
import {useDispatch, useSelector} from "react-redux";
import {createAccount, createAccountFailure} from "../store/actions/profileActions";
import FormSignUp from "../components/UI/Form/FormSignUp";

const SignUp = () => {
    const errAccount = useSelector((state) => state.profile.errAccount);
    const dispatch = useDispatch();
    const resetErrHandler = useCallback((key) => {
        dispatch(createAccountFailure({...errAccount, [key]: null}));
    }, [errAccount, dispatch])

    useEffect(() => () => {
        dispatch(createAccountFailure(null))
    }, [dispatch]);

    return (
        <Box justifyContent={"center"} width={'100%'} pt={8}>
            <FormSignUp
                actionName='Sign up'
                errors={errAccount}
                resetErrHandler={resetErrHandler}
                helperLinkName='Already have an account? Sign in'
                toLocation='/signin'
                onSubmit={(data) => dispatch(createAccount(data))}
            />
        </Box>
    );
};

export default SignUp;