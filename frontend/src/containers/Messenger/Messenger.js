import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Button, Grid, Paper, Stack, TextField, Tooltip, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {useSelector} from "react-redux";
import {BASE_URL} from "../../config";
import Avatar from "@mui/material/Avatar";

const useStyles = makeStyles({
    bgSection: {
        background: "white",
        border: '1px solid gainsboro',
    },
    bgContent: {
        minHeight: 50,
        background: 'rgb(233,249,253)',
        border: '1px solid gainsboro',
        borderRadius: '8px',
        padding: '5px',
        '&:hover': {
            cursor: 'pointer',
            background: 'rgb(224,255,221)',
        },
    },
    active: {
        backgroundColor: 'rgb(174,255,166)',
        '&:hover': {
            background: 'rgb(174,255,166)',
        },
    },
});

const Messenger = () => {
    const classes = useStyles();
    const profile = useSelector(state => state.profile.data);
    const [onlineUsers, setOnlineUsers] = useState(null);
    const [userMessages, setUserMessages] = useState(null);
    const [recipientUser, setRecipientUser] = useState(null);
    const [inpMessage, setInpMessage] = useState('');
    const ws = useRef(null);

    const setUserActive = useCallback((id) => {
        setRecipientUser(prevState => {
            if (prevState === id) return null;
            return id;
        })
    }, []);


    const onMsgSubmit = (e) => {
        e.preventDefault();
        const newMsg = {
            author: profile._id,
            message: inpMessage,
            datetime: new Date().toISOString(),
        };
        if (recipientUser) {
            newMsg.recipient = recipientUser;
        }
        ws.current.send(JSON.stringify({type: 'NEW_MESSAGE', data: newMsg}));
        setInpMessage('');
    }

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:9000/messages' + (profile?.token ? '?token=' + profile.token : ''));
        ws.current.onmessage = event => {
            const parsedMsg = JSON.parse(event.data);
            switch (parsedMsg.type) {
                case 'ONLINE_USERS':
                    setOnlineUsers(parsedMsg.onlineUsers);
                    break;
                case "MESSAGES":
                    setUserMessages(parsedMsg.data);
                    break;
                default:
                    return;
            }
        }
        return () => {
            ws.current.close();
        }
    }, [profile]);
    
    return (
        <Grid container height={'90vh'} spacing={1} mt={2} alignContent={"stretch"}>
            <Grid item display={"inline-flex"} xs={3}>
                <Stack className={classes.bgSection} p={1} flexGrow={1} spacing={2}>
                    <Typography variant={"h5"} textAlign={"center"}>
                        Online users:
                    </Typography>
                    {onlineUsers?.users && (
                        Object.keys(onlineUsers.users).map((user) => {
                            const {_id, username, image} = onlineUsers.users[user];
                            return (
                                <Tooltip title={recipientUser === _id ? 'Cancel selection' : 'Send message'}
                                         placement="right"
                                         key={_id}
                                >
                                    <Grid
                                        className={classes.bgContent + (recipientUser === _id ? ' ' + classes.active : '')}
                                        onClick={() => setUserActive(_id)}

                                        alignItems={'center'}
                                        container
                                    >
                                        <Grid item>
                                            <Avatar alt={username}
                                                    src={BASE_URL + image}
                                            />
                                        </Grid>
                                        <Grid item flexGrow={1} ml={'5%'}>
                                            <Typography variant={"body1"}

                                            >
                                                {username}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Tooltip>
                            );
                        })
                    )}
                    {onlineUsers?.anonymous && (
                        <Grid container className={classes.bgContent}
                              onClick={() => setRecipientUser(null)}
                              justifyContent={"center"}
                              alignItems={"center"}
                        >
                            <Typography variant={"body1"}
                                        textAlign={"center"}
                            >
                                Anonymous: {onlineUsers.anonymous.count}
                            </Typography>
                        </Grid>
                    )}
                </Stack>
            </Grid>
            <Grid item display={"inline-flex"} flexGrow={1}>
                <Stack flexGrow={1} spacing={1}>
                    <Stack className={classes.bgSection}
                           p={2} flexGrow={2}
                           spacing={2}
                           sx={{
                               overflowY: 'scroll',
                               overflowX: 'hidden',
                               height: '75vh',
                           }}>
                        {userMessages?.length > 0 && (
                            userMessages.map(message => {
                                return (
                                    <Grid container key={message._id} component={Paper} p={1} direction={"column"}>
                                        <Grid item textAlign={"center"}>
                                            <Avatar alt={message.author.username}
                                                    src={BASE_URL + message.author.image}
                                                    sx={{margin: '0 auto'}}
                                            />
                                            <Typography variant={'h6'}>
                                                {message.author.username}
                                            </Typography>
                                        </Grid>
                                        <Grid item p={1} sx={{borderTop: '1px solid gainsboro'}}>
                                            <Typography variant={'body1'}>
                                                {message.message}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                )
                            })
                        )}
                    </Stack>
                    <Stack className={classes.bgSection} p={1} minHeight={200} flexGrow={1}>
                        <Grid container
                              onSubmit={onMsgSubmit}
                              component={'form'}
                              justifyContent={'center'}
                              alignItems={'center'}
                              flexGrow={1}
                              spacing={2}
                        >
                            {profile ? (
                                <>
                                    <Grid item flexGrow={1} maxWidth={500}>
                                        <TextField
                                            value={inpMessage}
                                            autoComplete={'off'}
                                            onChange={(e) => setInpMessage(() => e.target.value)}
                                            variant={"filled"}
                                            label={recipientUser ? 'Send to ' + onlineUsers.users[recipientUser].username : 'Send'}
                                            required
                                        />
                                    </Grid>
                                    <Grid item>
                                        <Button type={'submit'}
                                                size={'large'}
                                                variant={'contained'}
                                        >
                                            Send
                                        </Button>
                                    </Grid>
                                </>
                            ) : (
                                <Grid item>
                                    <Typography variant={'h4'}>
                                        First log in to send a messages
                                    </Typography>
                                </Grid>
                            )}
                        </Grid>
                    </Stack>
                </Stack>
            </Grid>
        </Grid>
    );
};

export default Messenger;