import * as React from 'react';
import {useEffect, useState} from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import {Link as RouterLink} from 'react-router-dom';

const initState = {
    username: '',
    password: '',
    email: '',
    image: null,
};

const FormSignUp = ({actionName, helperLinkName, toLocation, onSubmit, errors, resetErrHandler}) => {
    const [state, setState] = useState(initState);
    const handleSubmit = (event) => {
        event.preventDefault();
        const newUser = new FormData();
        Object.keys(state).forEach(key => {
            newUser.append(key, state[key]);
        })
        onSubmit(newUser);
    };
    const handleInpChange = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };
    const formImgChanger = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.files[0]}));
    };

    useEffect(() => {
        for (let key of Object.keys(state)) {
            if (state[key] && errors?.[key]) resetErrHandler(key)
        }
    }, [state, errors, resetErrHandler]);
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
        >
            <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                <VpnKeyIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                {actionName}
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{mt: 1}}>
                <TextField
                    margin="normal"
                    label="Login"
                    name="username"
                    value={state.username}
                    onChange={handleInpChange}
                    error={Boolean(errors?.username)}
                    helperText={errors?.username?.message}
                    autoFocus
                    required
                />
                <TextField
                    margin="normal"
                    name="email"
                    label="Email"
                    value={state.email}
                    error={Boolean(errors?.email)}
                    helperText={errors?.email?.message}
                    onChange={handleInpChange}
                    required
                />
                <TextField
                    margin="normal"
                    name="password"
                    label="Password"
                    type="password"
                    error={Boolean(errors?.password)}
                    helperText={errors?.password?.message}
                    value={state.password}
                    onChange={handleInpChange}
                    required
                />
                <TextField
                    margin="normal"
                    type={'file'}
                    name="image"
                    onChange={formImgChanger}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
                >
                    {actionName}
                </Button>
                <Grid container direction={'row-reverse'}>
                    <Grid item>
                        <Link component={RouterLink} to={toLocation} variant="body2">
                            {helperLinkName}
                        </Link>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}

export default FormSignUp;