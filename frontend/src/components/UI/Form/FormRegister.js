import * as React from 'react';
import {useState} from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import {Link as RouterLink} from 'react-router-dom';

const initState = {
    username: '',
    password: '',
};

const FormRegister = ({actionName, helperLinkName, toLocation, onSubmit, errors}) => {
    const [state, setState] = useState(initState);
    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit(state);
    };
    const handleInpChange = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
        >
            <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                <VpnKeyIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                {actionName}
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{mt: 1}}>
                <TextField
                    margin="normal"
                    label="Login"
                    name="username"
                    value={state.username}
                    onChange={handleInpChange}
                    error={Boolean(errors?.username)}
                    helperText={errors?.username?.message}
                    autoFocus
                    required
                />
                <TextField
                    margin="normal"
                    name="password"
                    label="Password"
                    type="password"
                    error={Boolean(errors?.password)}
                    helperText={errors?.password?.message}
                    value={state.password}
                    onChange={handleInpChange}
                    required
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
                >
                    {actionName}
                </Button>
                <Grid container direction={'row-reverse'}>
                    <Grid item>
                        <Link component={RouterLink} to={toLocation} variant="body2">
                            {helperLinkName}
                        </Link>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}

export default FormRegister;