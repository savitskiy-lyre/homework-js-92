import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import SignIn from "./containers/SignIn";
import SignUp from "./containers/SignUp";
import Messenger from "./containers/Messenger/Messenger";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Messenger}/>
                <Route path="/signin" component={SignIn}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </Layout>
    );
};

export default App;
