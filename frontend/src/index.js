import store from "./store/configurateStore";
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import {ThemeProvider} from "@mui/material/styles";
import App from './App';
import history from "./history";
import {theme} from "./theme";

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <Router history={history}>
                <App/>
            </Router>
        </ThemeProvider>
    </Provider>
    , document.getElementById('root')
);
