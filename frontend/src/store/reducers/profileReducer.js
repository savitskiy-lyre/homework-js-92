import {
    CREATE_ACCOUNT_FAILURE,
    CREATE_ACCOUNT_REQUEST,
    CREATE_ACCOUNT_SUCCESS,
    SIGN_IN_FAILURE,
    SIGN_IN_REQUEST,
    SIGN_IN_SUCCESS,
    USER_LOGOUT
} from "../actions/profileActions";

const initState = {
    data: null,
    errLogIn: null,
    errAccount: null,
};

export const profileReducer = (state = initState, action) => {
    switch (action.type) {
        case USER_LOGOUT:
            return initState;
        case CREATE_ACCOUNT_REQUEST:
            return {...state, errAccount: null}
        case CREATE_ACCOUNT_SUCCESS:
            return {...state, data: action.payload}
        case CREATE_ACCOUNT_FAILURE:
            return {...state, errAccount: action.payload}
        case SIGN_IN_REQUEST:
            return {...state, errLogIn: null}
        case SIGN_IN_SUCCESS:
            return {...state, data: action.payload}
        case SIGN_IN_FAILURE:
            return {...state, errLogIn: action.payload}
        default:
            return state;
    }
}


